
#include <stdlib.h>
#include <stdio.h>

#include "test.h"

int testSwitch1 ( int n, int b)
{
    int r = b;

    switch (n) {
    case 0:
        r += 0x11;
        break;
    case 1:
        r += 0x22;
        break;
    case 2:
        r += 0x33;
        break;
    case 3:
        r += 0x44;
        break;
    case 4:
        r += 0x55;
        break;
    case 5:
        r += 0x66;
        break;
    default:
        r = 0;
        break;
    }

    return (r);
}

int testSwitch2 ( int n, int b)
{
    int r;

    switch (n) {
    case 0:
        r = b / 10;
        break;
    case 1:
        r = b - 9;
        break;
    case 2:
        r = b + 3;
        break;

    case 10:
        r = b;
        break;
    case 11:
        r = 5;
        break;
    case 12:
        r = b * 6;
        break;
    default:
        r = 0;
        break;
    }

    return (r);
}

int testSwitch3 ( int n, int b)
{
    int r;

    switch (n) {
    case 0:
        r = b / 10;
        break;
    case 1:
        r = b - 9;
        break;
    case 2:
        r = b + 3;
        break;

    case 10000:
        r = b;
        break;
    case 10001:
        r = 5;
        break;
    case 10002:
        r = b * 6;
        break;
    case 10003:
        r = b * 6;
        break;
    case 10004:
        r = b * 66;
        break;
    case 10005:
        r = b * 67;
        break;
    case 10006:
        r = b * 60;
        break;
    case 10007:
        r = b * 91;
        break;
    case 10008:
        r = b * 65;
        break;

    default:
        r = 0;
        break;
    }

    return (r);
}
