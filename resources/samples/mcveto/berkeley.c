//@summarystart
// This example is adapted from StInG
// Sriram Sankaranarayanan, Henny Sipma and Zohar Manna,
// "Constraint-based Linear-Relations Analysis", in Static Analysis Symposium, 2004
//@summaryend
#include "../include/reach.h"


int main()
{
  unsigned int invalid;
  unsigned int unowned;
  unsigned int nonexclusive;
  unsigned int exclusive;

  if ((exclusive==0) && (nonexclusive==0) && (unowned==0) && (invalid>= 1))
    {
      while (MakeChoice() & 1)
	{
	  if (MakeChoice() & 1)
	    {
	      if (invalid >= 1) {
		nonexclusive=nonexclusive+exclusive;
		exclusive=0;
		invalid=invalid-1;
		unowned=unowned+1; 
	      }
	    }
	  else
	    {
	      if (MakeChoice() & 1)
		{
		  if (nonexclusive + unowned >=1) {
		    invalid=invalid + unowned + nonexclusive-1;
		    exclusive=exclusive+1;
		    unowned=0;
		    nonexclusive=0; 
		  }
		}
	      else
		{
		  if (invalid >= 1) {
		    unowned=0;
		    nonexclusive=0;
		    exclusive=1;
		    invalid=invalid+unowned+exclusive+nonexclusive-1; 
		  }
		}
	    }
	}

      if (exclusive < 0 || unowned < 0 || invalid + unowned + exclusive + nonexclusive < 1) 
	REACHABLE();
    }

  return 0;
}

// predicates required will be among the following.
// exclusive >= 0 && unowned >= 0 &&  nonexclusive >= 0 && invalid + unowned + exclusive >= 1 &&
// 2*invalid + unowned + 2*exclusive >= 1
// This is the Loop invariant computed 
