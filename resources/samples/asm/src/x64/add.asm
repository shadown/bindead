
format elf64
	
section '.data' writable

foo		dw 0x2342
msg		db 'Hello World64!', 0x0A
msg_size	= $-msg

section '.text' executable

public main

main:
   add ah, bl
   add al, bl
   add ax, bx
   add eax, ebx
;   add bx, word [bx]
   add eax, dword [eax+eax*4+0x42]
   add al, byte [eax+eax*4+0x23]

   ; exit
   xor     edi, edi
   mov     eax, 60
   syscall
